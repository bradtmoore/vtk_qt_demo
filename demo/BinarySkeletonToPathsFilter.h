#ifndef BINARYSKELETONTOPATHSFILTER_H
#define BINARYSKELETONTOPATHSFILTER_H
#include "itkProcessObject.h"
#include "itkImage.h"
#include "itkPath.h"
#include "itkPolyLineParametricPath.h"
#include "itkSimpleDataObjectDecorator.h"
#include "itkNeighborhoodIterator.h"
#include "IndexHash.h"
#include "SkeletonNode.h"

#include <vector>
#include <unordered_set>
#include <memory>
#include <unordered_map>

template <typename TPixel>
class BinarySkeletonToPathsFilter : public itk::ProcessObject
{
public:
    typedef BinarySkeletonToPathsFilter Self;
    typedef itk::ProcessObject Superclass;
    typedef itk::SmartPointer< Self > Pointer;
    typedef TPixel PixelType;
    typedef itk::Image< PixelType, 2 > ImageType;
    typedef itk::PolyLineParametricPath< 2 > PathType;
    typedef itk::Index< 2 > IndexType;
    typedef std::vector< PathType::Pointer > PathCollection;
    typedef itk::SimpleDataObjectDecorator< PathCollection > PathCollectionDataObject; // and people say Java is verbose
    typedef SkeletonNode< IndexType > NodeType;

    itkNewMacro(Self);

    ProcessObject::DataObjectPointer MakeOutput(ProcessObject::DataObjectPointerArraySizeType idx);

    void SetInput(ImageType *image);
    ImageType* GetInput();

    PathCollectionDataObject* GetOutput();

    virtual void GenerateData();

protected:
    BinarySkeletonToPathsFilter();
    ~BinarySkeletonToPathsFilter();

};

template < typename TPixel >
itk::ProcessObject::DataObjectPointer
BinarySkeletonToPathsFilter< TPixel >::MakeOutput(ProcessObject::DataObjectPointerArraySizeType)
{
  return PathCollectionDataObject::New().GetPointer();
}

template < typename TPixel >
BinarySkeletonToPathsFilter< TPixel >::BinarySkeletonToPathsFilter() {
    typename PathCollectionDataObject::Pointer output =
      static_cast< PathCollectionDataObject * >( this->MakeOutput(0).GetPointer() );
    this->ProcessObject::SetNumberOfRequiredOutputs(1);
    this->ProcessObject::SetNthOutput( 0, output.GetPointer() );

    this->SetNumberOfRequiredInputs(1);

    // Set the default behavior of an image source to NOT release its
    // output bulk data prior to GenerateData() in case that bulk data
    // can be reused (an thus avoid a costly deallocate/allocate cycle).
    this->ReleaseDataBeforeUpdateFlagOff();
}

template < typename TPixel >
void BinarySkeletonToPathsFilter< TPixel >::GenerateData() {
    itk::Size< 2 > radius;
    radius.Fill(1);

    itk::ConstNeighborhoodIterator< ImageType > iter(radius, GetInput(), GetInput()->GetLargestPossibleRegion());
    std::unordered_map< IndexType, std::shared_ptr< NodeType >, IndexHash< IndexType > > nodeMap;
    std::vector< std::shared_ptr< NodeType > > endPoints;

    auto makeNode = [&nodeMap](IndexType index) {
        std::shared_ptr< NodeType > nodePtr;
        auto mapIter = nodeMap.find(index);
        if (mapIter != nodeMap.end()) {
            nodePtr = mapIter->second;
        }
        else {
            nodePtr = std::make_shared< NodeType >(index);
            nodeMap.insert(std::make_pair(index, nodePtr));
        }
        return nodePtr;
    };

    for (iter.GoToBegin(); ! iter.IsAtEnd(); ++iter) {
        if (iter.GetCenterPixel() > 0) {
            std::shared_ptr< NodeType > nodePtr = makeNode(iter.GetIndex());
            for (unsigned int i = 0; i < iter.Size(); i++) {
                bool inBounds;
                if (iter.GetPixel(i, inBounds) > 0 && inBounds) {
                    std::shared_ptr< NodeType > tmp = makeNode(iter.GetIndex(i));

                    if (*tmp != *nodePtr) {
                        nodePtr->AddNeighbor(tmp);
                    }
                }
            }
            if (nodePtr->GetType() == NodeType::ENDPOINT) {
                endPoints.push_back(nodePtr);
            }
        }
    }

    std::unordered_set< IndexType, IndexHash < IndexType > > endVisited;
    PathCollection &output = GetOutput()->Get();
    for (std::shared_ptr< NodeType > node : endPoints) {
        if (endVisited.find(node->GetIndex()) == endVisited.end()) {
            endVisited.insert(node->GetIndex());

            PathType::Pointer tmp = PathType::New();
            //tmp->Inititalize();
            tmp->AddVertex(node->GetIndex());

            std::shared_ptr< NodeType > n1 = node;
            std::shared_ptr< NodeType > n2 = node->Follow();
            while (n2->GetType() != NodeType::ENDPOINT) {
                tmp->AddVertex(n2->GetIndex());

                std::shared_ptr< NodeType > n3;
                n3 = n2->Follow(*n1);
                n1 = n2;
                n2 = n3;
            }
            tmp->AddVertex(n2->GetIndex());
            endVisited.insert(n2->GetIndex());
            output.push_back(tmp);
        }
    }
}

template < typename TPixel >
typename BinarySkeletonToPathsFilter< TPixel >::PathCollectionDataObject *BinarySkeletonToPathsFilter< TPixel >::GetOutput() {
    return static_cast< PathCollectionDataObject * >(this->GetPrimaryOutput());
}

template < typename TPixel >
void BinarySkeletonToPathsFilter< TPixel >::SetInput(ImageType *image) {
    this->SetNthInput(0, image);
}

template < typename TPixel >
BinarySkeletonToPathsFilter< TPixel >::~BinarySkeletonToPathsFilter() {

}

template < typename TPixel >
typename BinarySkeletonToPathsFilter< TPixel >::ImageType* BinarySkeletonToPathsFilter< TPixel >::GetInput() {
    return static_cast< ImageType* >(this->GetPrimaryInput());
}

#endif // BINARYSKELETONTOPATHSFILTER_H
