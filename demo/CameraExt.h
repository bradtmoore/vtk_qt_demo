#ifndef CAMERAEXT_H
#define CAMERAEXT_H

#include "vtkObject.h"
#include "vtkCamera.h"
#include "vtkSmartPointer.h"
#include "vtkSetGet.h"

/**
 * @brief Utility methods/extensions to vtkCamera.  Due to the factory pattern used in VTK, it doesn't
 * make sense to inherit from factory-based classes.
 */
class CameraExt
{
public:
    /**
     * @brief PanTo
     * @param vec2
     */
    static void PanTo(vtkCamera &camera, const double *newPoint);
};

#endif // PLANECAMERAMANIPULATOR_H
