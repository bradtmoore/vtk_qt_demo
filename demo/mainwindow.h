#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QSharedPointer>
#include <QMouseEvent>
#include "vtkSmartPointer.h"
#include "vtkRenderer.h"
#include "ImageSegmentationRenderer.h"
#include "QFileDialog"

// forward declaration of the class generation by parsing the .ui file
namespace Ui {
class MainWindow;
}

// appropriate to set slots and stuff here
namespace hardconcepts { // added this to wrap the window in the demo namespace
class MainWindow : public QMainWindow
{
    Q_OBJECT

private slots:
    void on_actionOpen_triggered();
    void on_actionExit_triggered();
    void on_imageWidget_mouseEvent(QMouseEvent* event);
    void on_zoomButton_clicked(bool checked);
    void on_panButton_clicked(bool checked);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QString fileName;
    ImageSegmentationRenderer renderer;
    // if I have shareable data, use QSharedPointer

};
}
#endif // MAINWINDOW_H
