#ifndef WORMFILTER_H
#define WORMFILTER_H

#include "itkProcessObject.h"
#include "itkPolyLineParametricPath.h"
#include "itkPoint.h"
#include "itkQuaternionRigidTransform.h"
#include "itkVector.h"
#include "itkMesh.h"
#include "itkSimpleDataObjectDecorator.h"
#include "itkAutomaticTopologyMeshSource.h"
#include "itkImage.h"
#include "itkIndex.h"

template < typename TImage >
class WormFilter : public itk::ProcessObject
{
public:
    typedef WormFilter< TImage > Self;
    typedef itk::ProcessObject Superclass;
    typedef itk::SmartPointer< Self > Pointer;

    typedef TImage ImageType;
    typedef itk::Index< 2 > IndexType;
    typedef itk::PolyLineParametricPath< 2 > PathType;
    typedef std::vector< PathType::Pointer > PathCollection;
    typedef itk::SimpleDataObjectDecorator< PathCollection > PathCollectionDataObject;
    typedef itk::Point< double, 3 > PointType;
    typedef itk::Vector< double, 3 > Vector3;
    typedef itk::Vector< double, 2 > Vector2;
    typedef itk::Mesh< typename ImageType::PixelType > MeshType;
    typedef std::vector < typename MeshType::Pointer >  MeshCollection;
    typedef itk::SimpleDataObjectDecorator< MeshCollection > MeshCollectionDataObject;
    typedef itk::AutomaticTopologyMeshSource< MeshType > MeshSourceType;

    itkNewMacro(Self);

    static PointType indexToPoint(const IndexType &index);
    static PointType rotate(const PointType &point, double angle, const Vector3 &axis, const PointType &center);
    static PointType perp2D(const PointType &point, const PointType &center);
    static PointType getBoundaryPoint(const PointType &point, const Vector3 &vector, ImageType *thresholdedImage);
    static void getRing(const PointType &prevP, const PointType &curP, ImageType *threshold, int count, std::vector<PointType> &output);
    static void makeMesh(const PointType &prevP, const std::vector< PointType > &ring, MeshSourceType &output);
    static void makeMesh(const std::vector< PointType > &ring1, const std::vector< PointType > &ring2, MeshSourceType &output);

    itk::ProcessObject::DataObjectPointer MakeOutput(ProcessObject::DataObjectPointerArraySizeType idx);

    virtual void SetInput(ImageType *binaryThreshold, PathCollectionDataObject *medialAxes);
    ImageType* GetInputThreshold();
    PathCollectionDataObject* GetInputMedialAxes();

    MeshCollectionDataObject* GetOutput();

    virtual void GenerateData();


protected:
    WormFilter();
    ~WormFilter();
};

template < typename TImage >
WormFilter< TImage >::WormFilter() {
    typename MeshCollectionDataObject::Pointer output =
      static_cast< MeshCollectionDataObject * >( this->MakeOutput(0).GetPointer() );
    this->ProcessObject::SetNumberOfRequiredOutputs(1);
    this->ProcessObject::SetNthOutput( 0, output.GetPointer() );

    this->SetNumberOfRequiredInputs(2);

    // Set the default behavior of an image source to NOT release its
    // output bulk data prior to GenerateData() in case that bulk data
    // can be reused (an thus avoid a costly deallocate/allocate cycle).
    this->ReleaseDataBeforeUpdateFlagOff();
}

template < typename TImage >
WormFilter< TImage >::~WormFilter() {

}

template < typename TImage >
typename WormFilter< TImage >::PointType WormFilter< TImage >::indexToPoint(const IndexType &index) {
    PointType::ValueType r[3];
    r[0] = index[0];
    r[1] = index[1];
    r[2] = 0;

    return PointType(r);
}

template < typename TImage >
typename WormFilter< TImage >::PointType WormFilter< TImage >::rotate(const PointType &point, double angle, const Vector3 &axis, const PointType &center) {
    itk::QuaternionRigidTransform<>::Pointer quaternion = itk::QuaternionRigidTransform<>::New();
//    double coef = std::sin(angle / 2.0);
//    itk::QuaternionRigidTransform<>::ParametersType::ArrayType::ValueType d1[7] = {
//        axis[0] * coef,
//        axis[1] * coef,
//        axis[2] * coef,
//        std::cos(angle/2.0),
//        center[0],
//        center[1],
//        center[2]};
//    itk::QuaternionRigidTransform<>::ParametersType::ArrayType q1(d1, 7);
//    itk::QuaternionRigidTransform<>::ParametersType p1(q1);

    vnl_vector_fixed< double, 3 > vnl_axis(axis.GetDataPointer());
    vnl_quaternion< double > vnl_quat(vnl_axis, angle);
    quaternion->SetRotation(vnl_quat);
    Vector3 vec(center.GetDataPointer());
    quaternion->SetCenter(center);
    //quaternion->SetTranslation(-vec);
    //quaternion->SetParameters(p1);
    //quaternion->
    return quaternion->TransformPoint(point);
}

template < typename TImage >
typename WormFilter< TImage >::PointType WormFilter< TImage >::perp2D(const PointType &point, const PointType &center) {
    double angle = itk::Math::pi / 2.0;
    itk::QuaternionRigidTransform<>::ParametersType::ArrayType::ValueType a[3] = {0, 0, 1.0};
    itk::QuaternionRigidTransform<>::ParametersType::ArrayType aa(a, 3);
    Vector3 axes(a);
    return rotate(point, angle, axes, center);
}

template < typename TImage >
typename WormFilter< TImage >::PointType WormFilter< TImage >::getBoundaryPoint(const PointType &point, const Vector3 &vector, ImageType *thresholdedImage) {
    Vector3 walk(point.GetDataPointer());

    itk::Index< 2 >::IndexValueType d[2] = {std::floor(walk[0]), std::floor(walk[1])};
    itk::Index< 2 > index;
    index.SetIndex(d);
    while (thresholdedImage->GetPixel(index) > 0) { // oh, probably would die if there was an object on the boundary
        walk = walk + vector;
        d[0] = std::floor(walk[0]);
        d[1] = std::floor(walk[1]);
        index.SetIndex(d);
    }

    return walk - vector;
}

template < typename TImage >
void WormFilter< TImage >::getRing(const PointType &prevP, const PointType &curP, ImageType *threshold, int count, std::vector<PointType> &output) {
    Vector3 ortho = perp2D(prevP, curP) - curP;
    ortho.Normalize();
    PointType bound = getBoundaryPoint(curP, ortho, threshold);

    Vector3 deriv = prevP - curP;
    deriv.Normalize();

    std::cout << "prevP: " << prevP <<"\ncurP: " << curP << "\nbound: " << bound << std::endl;

    double stepSize = itk::Math::pi * 2.0 / (double)count;
    for (int i = 0; i < count; ++i) {
        double angle = stepSize * (double)i;
        PointType r = rotate(bound, angle, deriv, curP);
        std::cout << "Ring " << std::to_string(i) << ": " << r << std::endl;
        output.push_back(r);
    }
}

template < typename TImage >
void WormFilter< TImage >::makeMesh(const PointType &prevP, const std::vector< PointType > &ring, MeshSourceType &output) {
    PointType p1;
    PointType p2;

    for (std::size_t i = 1; i < ring.size(); i++) {
        p1 = ring.at(i-1);
        p2 = ring.at(i);
        output.AddTriangle(
            output.AddPoint(prevP),
            output.AddPoint(p1),
            output.AddPoint(p2));
    }

    output.AddTriangle(
        output.AddPoint(prevP),
        output.AddPoint(ring.back()),
        output.AddPoint(ring.front()));
}

template < typename TImage >
void WormFilter< TImage >::makeMesh(const std::vector< PointType > &ring1, const std::vector< PointType > &ring2, MeshSourceType &output) {
    PointType p1, p2, p3, p4;

    for (std::size_t i = 1; i < ring1.size(); ++i) {
        p1 = ring1.at(i-1);
        p2 = ring1.at(i);
        p3 = ring2.at(i-1);
        p4 = ring2.at(i);
//        output.AddTriangle(
//                    output.AddPoint(p1),
//                    output.AddPoint(p2),
//                    output.AddPoint(p3));
//        output.AddTriangle(
//                    output.AddPoint(p1),
//                    output.AddPoint(p2),
//                    output.AddPoint(p3));

        output.AddQuadrilateral(
            output.AddPoint(p1),
            output.AddPoint(p3),
            output.AddPoint(p4),
            output.AddPoint(p2));
    }
    p1 = ring1.back();
    p2 = ring1.front();
    p3 = ring2.back();
    p4 = ring2.front();

    std::cout << "Quadrilateral: " << p1 << "," << p2 << "," << p3 << "," << p4 << std::endl;
//    output.AddTriangle(
//                output.AddPoint(p1),
//                output.AddPoint(p2),
//                output.AddPoint(p3));
//    output.AddTriangle(
//                output.AddPoint(p1),
//                output.AddPoint(p2),
//                output.AddPoint(p3));

    output.AddQuadrilateral(
                output.AddPoint(p1),
                output.AddPoint(p3),
                output.AddPoint(p4),
                output.AddPoint(p2));
}


template < typename TImage >
itk::ProcessObject::DataObjectPointer WormFilter< TImage >::MakeOutput(ProcessObject::DataObjectPointerArraySizeType idx) {
    return MeshCollectionDataObject::New().GetPointer();
}

template < typename TImage >
void WormFilter< TImage >::SetInput(ImageType *binaryThreshold, PathCollectionDataObject *medialAxes) {
    this->SetNthInput(0, binaryThreshold);
    this->SetNthInput(1, medialAxes);
}

template < typename TImage >
typename WormFilter< TImage >::ImageType* WormFilter< TImage >::GetInputThreshold() {
    return static_cast< ImageType* >(this->GetInput(0));
}

template < typename TImage >
typename WormFilter< TImage >::PathCollectionDataObject* WormFilter< TImage >::GetInputMedialAxes() {
    return static_cast< PathCollectionDataObject* >(this->GetInput(1));
}

template < typename TImage >
typename WormFilter< TImage >::MeshCollectionDataObject* WormFilter< TImage >::GetOutput() {
    return static_cast< MeshCollectionDataObject * >(this->GetPrimaryOutput());
}

template < typename TImage >
void WormFilter< TImage >::GenerateData() {
    for (PathType::Pointer p : GetInputMedialAxes()->Get()) {
        auto meshSource = itk::AutomaticTopologyMeshSource< itk::Mesh< typename ImageType::PixelType, 3 > >::New();
        PointType prevP = indexToPoint(p->EvaluateToIndex(0));

        const unsigned int pathStep = 5;
        const unsigned int ringSize = 8;
        PointType curP = indexToPoint(p->EvaluateToIndex(pathStep));

        std::vector< PointType > prevRing;
        std::vector< PointType > curRing;

        ImageType *threshold = GetInputThreshold();
        getRing(prevP, curP, threshold, ringSize, prevRing);
        makeMesh(prevP, prevRing, *meshSource);
        std::swap(prevP, curP);
        for (unsigned int i = 2*pathStep; i < p->EndOfInput() - pathStep; i += pathStep) {
            curP = indexToPoint(p->EvaluateToIndex(i));
            getRing(prevP, curP, threshold, ringSize, curRing);
            makeMesh(prevRing, curRing, *meshSource);

            std::swap(prevP, curP);
            std::swap(prevRing, curRing);
            curRing.clear();
        }

        curP = indexToPoint(p->EvaluateToIndex(p->EndOfInput()));
        makeMesh(curP, prevRing, *meshSource);

        GetOutput()->Get().push_back(meshSource->GetOutput());
        //std::cout << "Head: " << prevP << "\nNext: " << curP << "\nOrtho: " << ortho << "\nBoundary: " << bound << std::endl;
    }
}

#endif // WORMFILTER_H
