#ifndef IMAGESEGMENTATIONRENDERER_H
#define IMAGESEGMENTATIONRENDERER_H
#include "vtkRenderer.h"
#include "vtkImageActor.h"
#include "vtkSmartPointer.h"

/**
 * @brief The class contains a VTK renderer and coordinates loading and displaying a single image.
 * Currently, this exposes the raw renderer for the image.  In the future,
 * all functionaly would go through public methods (e.g. panning, zooming).
 *
 * @author Brad T. Moore <brad.t.moore@gmail.com>
 * @date 5/2/2017
 */
class ImageSegmentationRenderer
{
public:
    ImageSegmentationRenderer();
    ~ImageSegmentationRenderer();

    /**
     * @brief Loads a given image into the renderer (replaces an existing one).
     * @param[in] image
     */
    void setImage(vtkSmartPointer<vtkImageActor> &image);

    /**
     * @brief Loads a PNG image at the given file path.
     * @param[in] filePath
     */
    void loadImageFromFile(const char* filePath);

    /**
     * @brief Get the image renderer.  In the future, this would be protected.
     * @return
     */
    vtkSmartPointer<vtkRenderer> getRenderer();

    void update();
    //void panBy(double *vec);

    //void scaleTo(double scale);
    //double getScale();

protected:
    vtkSmartPointer<vtkImageActor> image;
    vtkSmartPointer<vtkRenderer> renderer;
};

#endif // IMAGESEGMENTATIONRENDERER_H
