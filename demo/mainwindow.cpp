#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include "QVTKWidget.h"
#include "vtkRenderer.h"
#include "vtkSmartPointer.h"
#include "vtkRenderWindow.h"
#include "vtkTIFFReader.h"
#include "vtkPNGReader.h"
#include "vtkImageActor.h"
#include "vtkImageViewer.h"
#include "vtkInteractorStyle.h"
#include "vtkInteractorStyleImage.h"
#include "QVTKInteractor.h"
#include <iostream>
#include <QMdiSubWindow>
#include <QLabel>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include "vtkCoordinate.h"
#include "ImageSegmentationRenderer.h"

using hardconcepts::MainWindow; // do this, or use the namespace explicitly

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::on_actionOpen_triggered() {
    this->fileName = QFileDialog::getOpenFileName(this, "Select PNG image", ".", "Image (*.png)");
    //this->fileName = QString("C:/Development/GUI_Image_CPP_Demo/resources/test4.png");
    this->ui->imageWidget->loadImageFromFile(fileName.toStdString().c_str());

}

void MainWindow::on_actionExit_triggered() {
    this->close(); // need to think more about the difference between this class as the ui member.
}

void MainWindow::on_imageWidget_mouseEvent(QMouseEvent* event) {
    std::stringstream s;
    s << "Widg (X: " << std::setw(5) << event->x() << std::setw(0)
      << ", Y: " << setw(5) << event->y() << setw(0) << ")";
    this->ui->statusbar->showMessage(QString(s.str().c_str())); // TODO: verify this isn't creating a memory leak
}

void MainWindow::on_zoomButton_clicked(bool checked)
{
    ui->imageWidget->zooming();
}

void MainWindow::on_panButton_clicked(bool checked)
{
    ui->imageWidget->panning();
}

MainWindow::~MainWindow()
{
    delete ui;
}
