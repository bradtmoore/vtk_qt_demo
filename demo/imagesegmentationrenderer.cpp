#include "imagesegmentationrenderer.h"
#include "vtkImageActor.h"
#include "vtkPNGReader.h"
#include "vtkSmartPointer.h"
#include "vtkOpenGLRenderer.h"
#include "vtkCamera.h"
#include <iostream>
#include <cstdio>

ImageSegmentationRenderer::ImageSegmentationRenderer()
{
    this->renderer = vtkSmartPointer<vtkRenderer>::New();
    vtkCamera *cam = this->renderer->MakeCamera();
    cam->ParallelProjectionOn();
    this->renderer->SetActiveCamera(cam);
}

ImageSegmentationRenderer::~ImageSegmentationRenderer()
{

}

void ImageSegmentationRenderer::setImage(vtkSmartPointer<vtkImageActor> &image) {
    if (this->image) {
        renderer->RemoveActor(image);
    }

    this->image = image;
    renderer->AddActor(image);
    //renderer->GetActiveCamera()->ParallelProjectionOn();
    renderer->ResetCamera();
}

void ImageSegmentationRenderer::loadImageFromFile(const char* filePath) {
    auto reader = vtkSmartPointer<vtkPNGReader>::New(); // TODO: surely there is some sort of factory object I should use
    reader->SetFileName(filePath);
    reader->Update(); // key to having this render properly, makes sense?

    auto image = vtkSmartPointer<vtkImageActor>::New();
    image->SetInputData(reader->GetOutput());
    setImage(image);
/*
    double *d2 = image->GetBounds();
    const size_t n = 1000;
    char str[n];
    snprintf(str, n, "image.Position: %f, %f\nimage.Center:%f, %f\nimage.Bounds: x:%f,%f; y:%f,%f; z:%f,%f",
             image->GetPosition()[0], image->GetPosition()[1],
             image->GetCenter()[0], image->GetCenter()[1],
             d2[0], d2[1], d2[2], d2[3], d2[4], d2[5]);
    std::cout << str << std::endl;
    */
}

void ImageSegmentationRenderer::update() {
    renderer->UpdateLightsGeometryToFollowCamera();
}

vtkSmartPointer<vtkRenderer> ImageSegmentationRenderer::getRenderer() {
    return renderer;
}
