#include "GTEngineBinaryThinningFilter.h"
#include "Imagics/GteImage2.h"
#include "Imagics/GteImageUtility2.h"
#include "itkImageRegionConstIteratorWithIndex.h"
#include "itkImageRegionIteratorWithIndex.h"

GTEngineBinaryThinningFilter::GTEngineBinaryThinningFilter()
{

}

GTEngineBinaryThinningFilter::~GTEngineBinaryThinningFilter()
{

}

void GTEngineBinaryThinningFilter::GenerateData()
{
    using GTEImage = gte::Image2< int >;

    this->AllocateOutputs();

    auto input = this->GetInput();
    auto output = this->GetOutput();

    auto region = input->GetLargestPossibleRegion();
    auto size = region.GetSize();

    GTEImage gteImage(size[0], size[1]);
    itk::ImageRegionConstIteratorWithIndex<InputImageType> iter(input, region);

    // will iterate through the image however ITK wants to
    // and will copy over to the gteImage so we can use its filter
    while (! iter.IsAtEnd()) {
        InputImageType::IndexType idx = iter.GetIndex();
        int x = idx[0];
        int y = idx[1];
        gteImage(x, y) = iter.Get();
        ++iter;
    }
    std::cout << "Got here!" << std::endl;
    gte::ImageUtility2::GetSkeleton(gteImage); // overwrites gteImage

    // again, iterating the ITK way but copying the values from gteImage
    auto region2 = output->GetLargestPossibleRegion();
    //output->SetBufferedRegion(region2);
    itk::ImageRegionIteratorWithIndex<InputImageType> iter2(output, region2);
    while (! iter2.IsAtEnd()) {
        InputImageType::IndexType idx = iter2.GetIndex();
        int x = idx[0];
        int y = idx[1];
        iter2.Set(gteImage(x, y));
        ++iter2;
    }
    std::cout << "So far so good!" << std::endl;
}
