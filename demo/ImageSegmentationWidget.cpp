#include "ImageSegmentationWidget.h"
#include "vtkCoordinate.h"
#include "vtkSmartPointer.h"
#include "vtkCamera.h"
#include <sstream>
#include <string>
#include <iomanip>
#include <cmath>

ImageSegmentationWidget::ImageSegmentationWidget(QWidget *parent) : QVTKWidget(parent)
{
    SetRenderWindow(window.getWindow());
    mode = UNLOADED;
    dragging = false;
    projRef = vtkSmartPointer<vtkMatrix4x4>::New();
    invProjRef = vtkSmartPointer<vtkMatrix4x4>::New();
}

void ImageSegmentationWidget::loadImageFromFile(const QString &filePath) {
    window.loadImageFromFile(filePath.toStdString().c_str());
    window.Render();
    initialScale = window.getRenderer().getRenderer()->GetActiveCamera()->GetParallelScale();
    dScale = std::log(initialScale) / 100;
    mode = PANNING;
}

void ImageSegmentationWidget::SetRenderWindow(vtkRenderWindow *renderWindow) {
    QVTKWidget::SetRenderWindow(renderWindow);
    renderWindow->SetInteractor(nullptr); // QVTKWidget tries to set a default interactor
}

void ImageSegmentationWidget::mousePressEvent(QMouseEvent *event) {
    auto r = window.getRenderer().getRenderer();

    if (mode == PANNING) {
        dragging = true;

        // set our world reference
        r->SetDisplayPoint(event->x(), event->y(), 0);
        r->DisplayToWorld();
        r->GetWorldPoint(worldRef);
    }
    else if (mode == ZOOMING && (event->button() == Qt::LeftButton || event->button() == Qt::RightButton)) {
        vtkCamera *cam = window.getRenderer().getRenderer()->GetActiveCamera();
        double scale = cam->GetParallelScale();
        double d = (event->button() == Qt::LeftButton)? -dScale : +dScale;
        double s = std::exp(std::log(scale) + d);
        cam->SetParallelScale(s);
        window.Render();
    }
}

void ImageSegmentationWidget::widgetToWorld(int wx, int wy, double &x, double &y) {
    vtkRenderer *renderer = window.getRenderer().getRenderer();
    renderer->SetDisplayPoint(wx, wy, 0);
    renderer->DisplayToView();
    double *d = renderer->GetViewPoint();
    double pt1[4] = {d[0], d[1], 1, 1};
    double pt2[4];
    invProjRef->MultiplyPoint(pt1, pt2);
    x = pt2[0];
    y = pt2[1];
    //std::cout << "Inv: " << pt2[0] << "," << pt2[1] << std::endl;
//    auto coord = vtkSmartPointer<vtkCoordinate>::New();
//    coord->SetCoordinateSystemToViewport();
//    coord->SetValue(wx, wy);

//    double* val = coord->GetComputedWorldValue(window.getRenderer().getRenderer());
//    std::cout << "Coord: " << val[0] << "," << val[1] << std::endl;
//    coord->S
//    x = val[0];
//    y = val[1];
}

void ImageSegmentationWidget::mouseReleaseEvent(QMouseEvent *event) {
    dragging = false;
}

void ImageSegmentationWidget::mouseMoveEvent(QMouseEvent *event) {
    if (mode == PANNING && dragging) {
        double x[2];
        widgetToWorld(event->x(), event->y(), x[0], x[1]);
        double md[2] = {event->x() - mouseRef[0], event->y() - mouseRef[1]};
        //std::cout << "MouseDelta: " << md[0] << "," << md[1] << std::endl;

        double d[2] = {x[0] - worldRef[0], x[1] - worldRef[1]};
        //std::cout << "Delta: " << d[0] << "," << d[1] << std::endl;
        window.getRenderer().getRenderer()->GetActiveCamera()->SetPosition(cameraRef[0] - d[0], cameraRef[1] + d[1], cameraRef[2]);
        window.getRenderer().getRenderer()->GetActiveCamera()->Render(window.getRenderer().getRenderer());
        //window.getRenderer().getRenderer()->GetActiveCamera()->SetP

        //window.getRenderer().getRenderer()->GetActiveCamera()->UpdateViewport();
        window.getRenderer().getRenderer()->GetActiveCamera()->SetFocalPoint(focalRef[0] - d[0], focalRef[1] + d[1], focalRef[2]);
        window.Render();
    }


//    auto coord = vtkSmartPointer<vtkCoordinate>::New();
//    coord->SetValue(event->x(), event->y(), 0);
//    coord->SetCoordinateSystemToDisplay();
//    double *value = coord->GetComputedWorldValue(window.getRenderer().getRenderer());
//    std::stringstream s;
//    s << "Widg (X: " << std::setw(5) << event->x() << std::setw(0)
//      << ", Y: " << setw(5) << event->y() << setw(0)
//      << "), Image (X:" << setw(5) << value[0] << setw(0)
//      << ", Y: " << setw(5) << value[1] << setw(0) << ")";
//    //this->ui->statusbar->showMessage(QString(s.str().c_str())); // gah, why am i thinking memory leak here?
    emit mouseEvent(event); // TODO: why is this necessary to propogate the event?
}

void ImageSegmentationWidget::zooming() {
    if (mode != UNLOADED) {
        mode = ZOOMING;
    }
}

void ImageSegmentationWidget::panning() {
    if (mode != UNLOADED) {
        mode = PANNING;
    }
}

void ImageSegmentationWidget::resizeEvent(QResizeEvent *event) {
    QVTKWidget::resizeEvent(event);
    window.Render();
}
