#include "CameraExt.h"

void CameraExt::PanTo(vtkCamera &camera, const double *newPoint) {
    double focalPoint[3];
    double cameraPosition[3];
    camera.SetFocalPoint(focalPoint);
    camera.GetPosition(cameraPosition);

    // need to pan both the camera and the focal point within the view/camera plane
    double motion[3]; // motion within the camera plane
    double newFocalPoint[3];
    vtkMath::Subtract(newPoint, cameraPosition, motion);
    vtkMath::Add(focalPoint, motion, newFocalPoint);
    camera.SetPosition(newPoint);
    camera.SetFocalPoint(newFocalPoint);
}

