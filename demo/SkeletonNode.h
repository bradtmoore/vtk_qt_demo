#ifndef SKELETONNODE_H
#define SKELETONNODE_H

#include <vector>
#include <memory>

template < typename TIndex >
class SkeletonNode
{
public:
    static const int EDGE = 1;
    static const int ENDPOINT = 2;

    typedef TIndex IndexType;
    typedef std::shared_ptr< SkeletonNode > SharedPointer;

    SkeletonNode(IndexType index);
    ~SkeletonNode();

    int GetType();
    const IndexType GetIndex() const;
    void AddNeighbor(SharedPointer neighbor);

    SharedPointer Follow();
    SharedPointer Follow(SkeletonNode &prev);

    bool operator==(const SkeletonNode &rhs) const;
    bool operator!=(const SkeletonNode &rhs) const;

protected:
    int type;
    const IndexType index;
    std::vector< SharedPointer > neighbors;
};

template < typename TIndex >
SkeletonNode< TIndex >::SkeletonNode(IndexType index) : index(index) {

}

template < typename TIndex >
SkeletonNode< TIndex >::~SkeletonNode() {

}

template < typename TIndex >
int SkeletonNode< TIndex >::GetType() {
    return type;
}

template < typename TIndex >
const typename SkeletonNode< TIndex >::IndexType SkeletonNode< TIndex >::GetIndex() const {
    return index;
}

template < typename TIndex >
void SkeletonNode< TIndex >::AddNeighbor(SharedPointer neighbor) {
    neighbors.push_back(neighbor);
    type = (neighbors.size() == 1) ? ENDPOINT : EDGE;
}

template < typename TIndex >
typename SkeletonNode< TIndex >::SharedPointer SkeletonNode< TIndex >::Follow() {
    return neighbors.at(0);
}

template < typename TIndex >
typename SkeletonNode< TIndex >::SharedPointer SkeletonNode< TIndex >::Follow(SkeletonNode &prev) {
    SharedPointer ans;
    for (SharedPointer n : neighbors) {
        if (*n != prev) {
            ans = n;
            break;
        }
    }
    return ans;
}

template < typename TIndex >
bool SkeletonNode< TIndex >::operator==(const SkeletonNode &rhs) const {
    return rhs.index == index;
}

template < typename TIndex >
bool SkeletonNode< TIndex >::operator!=(const SkeletonNode &rhs) const {
    return ! (rhs == *this);
}

#endif // SKELETONNODE_H
