#ifndef IMAGESEGMENTATIONWIDGET_H
#define IMAGESEGMENTATIONWIDGET_H
#include <QWidget>
#include <QMouseEvent>
#include "QVTKWidget.h"
#include "ImageSegmentationWindow.h"
#include "vtkMatrix4x4.h"


/**
 * @brief A widget that contains a VTK RenderWindow and displays a rendered image, allowing for panning and zooming with the mouse.
 * It overrides QVTKWidget, as well as handles the coordination of Qt signals to modifying the underlying VTK pipeline directly.  Note,
 * this is different from QVTKWidget that defaults to using a QVTKInteractor to coordinate mouse events.  I'm not sold on
 * whether the abstraction away from the window system is worth it.
 *
 * @author Brad T. Moore <brad.t.moore@gmail.com>
 * @date 5/2/2017
 */
class ImageSegmentationWidget : public QVTKWidget
{
    Q_OBJECT
public:
    ImageSegmentationWidget(QWidget *parent);
    void SetRenderWindow(vtkRenderWindow *renderWindow);

public slots:
    /**
     * @brief Load an image (PNG) from the specified filePath and render it.
     * @param filePath
     */
    void loadImageFromFile(const QString &filePath);

    /**
     * @brief Change the widget's state to zooming, modifying how mouseEvents are handled.
     */
    void zooming();

    /**
     * @brief Change the widget's state to panning, modifying how mouseEvents are handled.
     */
    void panning();

protected:
    enum { // mode we are in
        UNLOADED,
        PANNING,
        ZOOMING,
        PAINTING
    };
    int mode;
    bool dragging;
    int mouseRef[2]; // reference point for when the mouse is dragged (initial click piont)
    double worldRef[4]; // reference point in world coordinates
    double focalRef[3]; // reference position of focus
    double cameraRef[3]; // reference position of camera
    double initialScale; // beginning scale of the image, used to determine the scale of zooming with each click
    double dScale; // amount of scale change per click

    vtkSmartPointer<vtkMatrix4x4> projRef; // reference projection matrix
    vtkSmartPointer<vtkMatrix4x4> invProjRef; // inverse reference projection matrix

    ImageSegmentationWindow window;
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    void printLocation(QMouseEvent *event);

    /**
     * @brief Convert widget mouse location (e.g. event->x()) to world coordinates
     * @param[in] wx Relative widget mouse location x
     * @param[in] wy Relative widget mouse location y
     * @param[out] x World coordinate
     * @param[out] y World coordinate
     */
    void widgetToWorld(int wx, int wy, double &x, double &y);
    void resizeEvent(QResizeEvent *event);
};

#endif // IMAGESEGMENTATIONWIDGET_H
