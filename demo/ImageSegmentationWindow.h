#ifndef IMAGESEGMENTATIONWINDOW_H
#define IMAGESEGMENTATIONWINDOW_H
#include "vtkRenderWindow.h"
#include "ImageSegmentationRenderer.h"

/**
 * @brief Coordinates an interactive image window.  Currently, only displays a single image, allowing for panning and zooming.
 * In the future, it would contain other renderers (one for segmentation, possibly one to display the pan location of a zoomed
 * image).  As is, I'm allowing for raw access to the VTK RenderWindow and Renderer, but in the future operations would be done
 * through public methods.
 *
 * @author Brad T. Moore <brad.t.moore@gmail.com>
 * @date 5/2/2017
 */
class ImageSegmentationWindow
{
public:
    ImageSegmentationWindow();

    /**
     * @brief Render this window.  Call after any updates.
     */
    void Render();

    /**
     * @brief Get the underlying vtkRenderWindow
     * @return
     */
    vtkSmartPointer<vtkRenderWindow> getWindow();

    /**
     * @brief Load a new image into the window.
     * @param file Path to a PNG image.
     */
    void loadImageFromFile(const char *file);
    ImageSegmentationRenderer& getRenderer();

protected:
    ImageSegmentationRenderer renderer;
    vtkSmartPointer<vtkRenderWindow> window;
};

#endif // IMAGESEGMENTATIONWINDOW_H
