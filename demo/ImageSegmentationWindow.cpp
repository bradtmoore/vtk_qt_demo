#include "ImageSegmentationWindow.h"

ImageSegmentationWindow::ImageSegmentationWindow()
{
    this->window = vtkSmartPointer<vtkRenderWindow>::New();
    this->window->AddRenderer(renderer.getRenderer());

    //auto interactor = vtkSmartPointer<QVTKInteractor>::New();
    //interactor->SetRenderWindow(renderWindow);
    //auto style = vtkSmartPointer<vtkInteractorStyleImage>::New();
    //interactor->SetInteractorStyle(style);
    //interactor->Initialize();

    //this->ui->qvtkWidget->SetRenderWindow(renderWindow);
    //renderWindow->SetInteractor(nullptr);
    //renderWindow->Render();
}

vtkSmartPointer<vtkRenderWindow> ImageSegmentationWindow::getWindow() {
    return window;
}

void ImageSegmentationWindow::Render() {
    window->Render();
}

void ImageSegmentationWindow::loadImageFromFile(const char *filePath) {
    renderer.loadImageFromFile(filePath);
}

ImageSegmentationRenderer& ImageSegmentationWindow::getRenderer() {
    return renderer;
}
