#include "demo/mainwindow.h"
#include <QApplication>
#include <QMessageBox>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    hardconcepts::MainWindow w;
    w.show();

    QMessageBox::information(&w, "Welcome!",
                             "VTK and Qt Demo:\n"
                             "\tClick File->Open to open a .png file, tested with example image"
                             "\tPanning and Zooming are enabled by pressing the P and Z toolbars.\n"
                             "\tTo Pan: Left-click and drag the mouse\n"
                             "\tTo Zoom: Left-click zoom in, Right-click out\n"
                             "\tClick File->Exit to exit\n"
                             "\nSummary:\n"
                             "This example shows the coordination of Qt with VTK.  I overwrote the "
                             "QVTKWidget wrote interactions as Qt slots maniuplating the renderer "
                             "and data directly.  An alternative would have been to write my own "
                             "vtkInteractor and vtkInteractorStyle.",
                             QMessageBox::Ok);
    return a.exec();
}
