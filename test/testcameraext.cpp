#include "demo/CameraExt.h"
#include <gtest/gtest.h>

TEST(CameraExt, positive) {
        EXPECT_EQ(4,4);
}

TEST(CameraExt, negative) {
        EXPECT_EQ(3,5);
}

int main(int argc, char **argv) {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
}
