#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkSmartPointer.h"
#include "itkBinaryMorphologicalClosingImageFilter.h"
#include "itkFlatStructuringElement.h"
#include "itkBinaryThinningImageFilter.h"
#include "itkImageToPathFilter.h"
#include "itkBSplineTransform.h"
#include "itkPath.h"
#include "itkContinuousIndex.h"
#include "demo/GTEngineBinaryThinningFilter.h"
#include "demo/BinarySkeletonToPathsFilter.h"
#include "itkQuaternionRigidTransform.h"
#include "itkPoint.h"
#include "itkVector.h"
#include "itkMath.h"
#include "itkMeshFileWriter.h"
#include "demo/WormFilter.h"
#include <exception>
#include <iostream>
#include <unordered_map>
#include <cmath>

using Image8bit = itk::Image<unsigned char, 2>;
using ImageReader8bit = itk::ImageFileReader<Image8bit>;
using BinaryThreshold = itk::BinaryThresholdImageFilter<Image8bit, Image8bit>;
using StructuringElementType = itk::FlatStructuringElement<2>;
using BinaryCloser = itk::BinaryMorphologicalClosingImageFilter<Image8bit, Image8bit, StructuringElementType>;
using ImageWriter8bit = itk::ImageFileWriter<Image8bit>;
using Skeletonizer = itk::BinaryThinningImageFilter<Image8bit, Image8bit>;
using Point2D = itk::ContinuousIndex<double, 2>;
using Path2D = itk::Path<double, Point2D, 2>;
using ImageToPath = itk::ImageToPathFilter<Image8bit, Path2D>;


int main(int argc, char *argv[]) {
    try {
        auto reader = ImageReader8bit::New();
        reader->SetFileName("C:/Development/GUI_Image_CPP_Demo/resources/test4.png");

        auto threshold = BinaryThreshold::New();
        threshold->SetInput(reader->GetOutput());
        threshold->SetLowerThreshold(0);
        threshold->SetUpperThreshold(147);
        threshold->SetInsideValue(255);
        threshold->SetOutsideValue(0);

        auto closer = BinaryCloser::New();

        StructuringElementType::RadiusType radius;
        radius.Fill(25);

        auto kernel = StructuringElementType::Ball(radius);

        closer->SetKernel(kernel);
        closer->SetInput(threshold->GetOutput());

        auto skeletonizer = GTEngineBinaryThinningFilter::New();
        skeletonizer->SetInput(closer->GetOutput());
        // OK, for this demo, it looks like the skeleton is a single path (surpisingly).
        // There is a pruning image filter in ITK I could use if that wasn't the case

        // normally, this is the part where I would extend the tips along their tangent
        // to the boundary of the worm

        auto pathsFilter = BinarySkeletonToPathsFilter< unsigned char >::New();
        pathsFilter->SetInput(skeletonizer->GetOutput());
        pathsFilter->Update();

        auto writer = ImageWriter8bit::New();
        writer->SetInput(skeletonizer->GetOutput());
        writer->SetFileName("./out.png");
        writer->Update();

        auto wormFilter = WormFilter< Image8bit >::New();

        wormFilter->SetInput(closer->GetOutput(), pathsFilter->GetOutput());
        wormFilter->Update();

        auto meshWriter = itk::MeshFileWriter< WormFilter< Image8bit >::MeshType >::New();
        auto meshes = wormFilter->GetOutput()->Get();
        for (int i = 0; i < meshes.size(); ++i) {
            meshWriter->SetInput(meshes.at(i));
            meshWriter->SetFileName("./mesh" + std::to_string(i) + ".byu");
            meshWriter->Write();
        }
//        itk::QuaternionRigidTransform<>::Pointer quaternion = itk::QuaternionRigidTransform<>::New();
//        double angle = itk::Math::pi / 4.0;
//        itk::QuaternionRigidTransform<>::ParametersType::ArrayType::ValueType d1[7] = {0, 0, std::sin(angle/2.0), std::cos(angle/2.0), 0, 0, 0};
//        itk::QuaternionRigidTransform<>::ParametersType::ArrayType q1(d1, 7);
//        itk::QuaternionRigidTransform<>::ParametersType p1(q1);
//        itk::QuaternionRigidTransform<>::InputPointValueType v1[3] = {1, 1, 1};
//        itk::QuaternionRigidTransform<>::InputPointType x1(v1);

//        quaternion->SetParameters(p1);
//        itk::QuaternionRigidTransform<>::OutputPointType o1 = quaternion->TransformPoint(x1);

//        std::cout << "Output:" << o1 << std::endl;

    }
    catch (const std::exception &err) {
        std::cout << err.what() << std::endl;
    }
    catch (...) {
        std::cout << "Unspen";
    }

    return 0;
}
