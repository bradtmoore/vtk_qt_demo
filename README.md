Checkout the summary [video](./VTK_Qt_Demo.mp4)

The goal of this demo is:
  1. Demonstrate a full-stack image rendering application: GUI (Qt/VTK)
  2. Provide a template for a professional C++ project using CMake
    a.  Debugger support
    b.  Qt Creator form designer support
    c.  (TODO) Unit-testing
    d.  (TODO) Doxygen support

In general, I've tried to expose the necessary rendering machinery from VTK to Qt's event-handling system.  There's a couple
of things I would probably do differently next time.  For example, a utility class for setting up and manipulating a VTK camera/renderer
that is viewing an image.  The existing VTK defaults for Image rendering are somewhat hard to extend as is.  I'd love to hear thoughts on 
whether VTK's event-processing should be used along with Qt's.  My usual instinct is to allow the parent application to handle event-processing.


Requirements
======================

1. VTK 7.0.0
2. Qt 5.8
3. GoogleTest